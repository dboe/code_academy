# Code Academy

This is a collection of teaching material for learning good programming practices.

# Presentations

* You can find presentation notebooks in ```presentations/<name>.ipynb```
* The corresponing html export is under ```presentations/html/<name>.html``` as source code or published by ci under the gitlab pages (coming soon)
