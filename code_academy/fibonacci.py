import numpy as np
import logging


LOGGER = logging.getLogger(__name__)


def fib(n):
    """
    Calculates the n-th Fibonacci number iteratively:

    $ F_n = {
        0, n = 0;
        1, n = 1;
        F_{n-1} + F_{n-2};
    }
    $

    Examples:
        >>> fib(10)
        55
    """
    LOGGER.info("Calculation of recursive fibonacci(%d).", n)
    a, b = 0, 1
    for _ in range(n):
        a, b = b, a + b
    # correct extension (not yet implemented)
    # for _ in range(abs(n)):
    #     if n < 0:
    #         b, a = a, b - a
    #     else:
    #         a, b = b, a + b
    return a


GOLDEN_RATIO = (1 + np.sqrt(5)) / 2  # golden ratio


def binets_formula(n):
    """
    Non recursive calculation from Binet (1843)

    Examples:
        >>> binets_formula(10)
        55
    """
    LOGGER.info("Calculation of binet fibonacci(%d).", n)
    # bf = (GOLDEN_RATIO**n - (-GOLDEN_RATIO)**(-n)) / np.sqrt(5)
    bf = (GOLDEN_RATIO ** n - np.cos(n * np.pi) * GOLDEN_RATIO ** (-n)) / np.sqrt(5)
    return int(round(bf, 10))