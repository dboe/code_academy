def fib(n):
    """
    Calculates the n-th Fibonacci number iteratively

    >>> fib(0)
    0
    >>> fib(1)
    1
    >>> fib(10)
    55
    >>> fib(15)
    610

    """
    a, b = 1, 1  # this is an error it should be 0, 1
    for i in range(n):
        a, b = b, a + b
    return a

if __name__ == "__main__":
    import doctest
    doctest.testmod()  # test all docstrings
    # doctest.run_docstring_examples(fib, globals())  # test one specific docstring
