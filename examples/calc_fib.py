import code_academy.fibonacci


def fun():
    n = -6
    return code_academy.fibonacci.fib(n)

if __name__ == "__main__":
    res = fun()
    assert res == 8, f"Results is {res} instead of 8"