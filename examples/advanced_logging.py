import logging
import code_academy.fibonacci


class ColoredFormatter(logging.Formatter):
    GREY = "\x1b[38;21m"
    YELLOW = "\x1b[33;21m"
    RED = "\x1b[31;21m"
    BOLD_RED = "\x1b[31;1m"
    RESET = "\x1b[0m"
    
    COLORS = {
        logging.DEBUG: GREY,
        logging.INFO: GREY,
        logging.WARNING: YELLOW,
        logging.ERROR: RED,
        logging.CRITICAL: BOLD_RED,
    }    
    def format(self, record):
        return self.COLORS.get(record.levelno) \
            + super().format(record) + self.RESET


def calculate_stuff():
    logger = logging.getLogger("Other_module")
    code_academy.fibonacci.fib(5)
    logger.critical("Critical error")


def main():
    logger = logging.getLogger()  # This is the root logger. Every other logger 
    logger.setLevel(logging.DEBUG)

    formatter = ColoredFormatter("%(asctime)s - LoggingDemo.ipynb:%(lineno)d - %(name)s - %(levelname)s: %(message)s")

    terminal_handler = logging.StreamHandler()  # Handlers can help directing your messages (e.g. file, terminal, mail, ...)
    terminal_handler.setLevel(logging.WARNING)

    file_handler = logging.FileHandler(filename="./records.log", mode='w')
    file_handler.setLevel(logging.DEBUG)


    terminal_handler.setFormatter(formatter)
    file_handler.setFormatter(formatter)

    logger.addHandler(terminal_handler)
    logger.addHandler(file_handler)
    
    calculate_stuff()
    

if __name__ == "__main__":
    main()