import unittest
from code_academy.fibonacci import fib, binets_formula


class FibTest(unittest.TestCase):
    def test_fib(self):
        for n in range(11):
            self.assertEqual(fib(n), binets_formula(n))
            self.assertIsInstance(fib(n), int)


if __name__ == '__main__':
    unittest.main()