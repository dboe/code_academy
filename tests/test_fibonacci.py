import unittest
from code_academy.fibonacci import fib, binets_formula


class FibTest(unittest.TestCase):
    def setUp(self) -> None:
        self.ns = range(20)
        self.results = [
            0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89,
            144, 233, 377, 610, 987, 1597, 2584, 4181
        ]  # first 20 numbers
        self.method = fib 

    def test_fibonacci(self):
        for n, res in zip(self.ns, self.results):
            expected = self.method(n)
            self.assertEqual(expected, res)
            self.assertIsInstance(expected, int)

    def test_negative_generalization(self):
        """
        $F_{-n} = (-1)^{n + 1} F_n
        """
        for n, res in zip(self.ns, self.results):
            expected = self.method(-n)
            res = (-1)**(n + 1) * res
            self.assertEqual(expected, res)
            self.assertIsInstance(expected, int)


class BinetTest(FibTest):
    def setUp(self) -> None:
        super().setUp()
        self.method = binets_formula


if __name__ == '__main__':
    unittest.main()